//data-parent="currency-cont" data-validate-trigger="next-btn" data-validate="empty"
(function (window, document, $, undefined) {
	
	
    var app = {
				
        WINDOW_HEIGHT: $(window).height(),
        WINDOW_WIDTH: $(window).width(),
        isMobile: false,
        isTouch: false,
        isTablet: false,
        resizeTimeoutID: null,
        $body: $("body"),
        isIe: false,
        detectDevice: function () {
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
                    app.isMobile = true;
                }
            })(navigator.userAgent || navigator.vendor || window.opera);
            if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
                app.isTouch = true;
                app.$body.addClass("touch");
            } else {
                app.$body.addClass("no-touch");
            }
            app.isTablet = (!app.isMobile && app.isTouch);
        },
        _windowResize: function () {
            // app.sliderEventOnlyMobile();
        },
        resizeListener: function () {
            $(window).resize(function () {
                clearTimeout(app.resizeTimeoutID);
                app.resizeTimeoutID = setTimeout(app._windowResize, 500);
            });
        },
        msIeVersion: function () {
            var ua = window.navigator.userAgent,
                msie = ua.indexOf("MSIE ");
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                $("body").addClass("ie");
                app.isIe = true
            }
            return false;
        },
        eventListeners: function () {
					
						var currentUrl,res;
						$("#langSwitch").click(function (e) {
						console.log(e.currentTarget.innerHTML);
						console.log(e);
						currentUrl = window.location.href;
							if(e.currentTarget.innerText == "English")
							{
								res = currentUrl.replace("/Ar/", "/en/");
								location.href = res;								
								console.log('resFromArabic: ' + res);
							}
							else
							{ 
						
								res = currentUrl.replace("/en/", "/Ar/");
								location.href = res;
								console.log('resFromEnglish: ' + res);
							}
						});
            $("#back").click(function () {
                window.history.back();
            });
            $(".close-btn").click(function () {
                $(".overlay").css("display", "none");
            });
            var productWidget = $('.product-collapse');
            productWidget.on('shown.bs.collapse', function () {
                $(this).parent().addClass("opened")
            });
            productWidget.on('hidden.bs.collapse', function () {
                $(this).parent().removeClass("opened");
            });
            var touchPointWidget = $('.touchpoint-collapse');
            touchPointWidget.on('shown.bs.collapse', function () {
                if (app.isMobile) {
                    $("html, body").animate({
                        scrollTop: $(this).parent().offset().top - 60
                    }, 300);
                }
                if ($(this).attr("id") === "touch-point-1") {
                    $(".outer-summary").hide();
                }
            });
            touchPointWidget.on('hide.bs.collapse', function () {
                if ($(this).attr("id") === "touch-point-1") {
                    $(".outer-summary").show();
                }
            });
            //productWidget.on('hidden.bs.collapse', function () {
            //    $(this).parent().removeClass("opened");
            //});
            $(".sticky-btn").click(function () {
                $(".sticky-btn").removeClass("active");
                $(this).addClass("active");
                $(".panel-in").hide();
                $(".panel-in").eq($(this).index()).show();
            });
            $(".side-panel .close-btn").click(function () {
                $(".panel-in").hide();
                $(".sticky-btn").removeClass("active");
            });
            $('.sticky-btn').click(function () {
                var scrollValue = $(".side-panel").offset().top;
                $("html, body").animate({
                    scrollTop: scrollValue - 50
                }, 600);
                //return false;
            });
            $('.touch .log-count').click(function () {
                if ($(window).width() <= 1000) {
                    $('.sticky-bar .sticky-ifno').addClass("active");
                    $(".panel-in").hide();
                    $(".panel-in.accordion-fill").show();
                    var scrollValue = $(".side-panel").offset().top;
                    $("html, body").animate({
                        scrollTop: scrollValue - 50
                    }, 600);
                    return false;
                }
            });
            $('.bundle-selector ul li.active').addClass('scroll-step');
            $('.scroll-step').click(function () {
                var targetId = $(this).find('a').attr('href');
                if (targetId !== 'javascript:void(0)') {
                    var idOffSetValue = $(targetId).offset().top;
                }
                $('html, body').animate({
                    scrollTop: idOffSetValue
                }, 700);
            });
            $(".bundle-selector ul li:not('.scroll-step'), .box-cont .row .col-md-6,   .products-slider li").click(function () {
                window.location = $(this).find("a").attr("href");
                return false;
            });
            $(".bundle-table table tbody tr td input[type=radio]").change(function () {
                var tdIndex = $(this).parent("td").index();
                $(".bundle-table table tbody tr td").removeClass("active");
                $(".bundle-table table tbody tr").each(function () {
                    $(this).find("td").eq(tdIndex - 1).addClass("active")
                });
            })
            $(".bundle-table-mob input[type=radio]").change(function () {
                var index = $(this).parents(".bundle-wrap").index();
                $(".bundle-table-mob .bundle-wrap").removeClass("active");
                $(".bundle-table-mob .bundle-wrap").eq(index).addClass('active');
            })
        },
        theme: function () {
            if (app._getQueryStringParams("theme") === "islamic") {
                $("body").addClass("theme-islamic");
                $("img").each(function () {
                    if ($(this).attr("data-islamic-img")) {
                        $(this).attr("src", $(this).attr("data-islamic-img"));
                        console.log("theme islamic")
                    }
                })
            } else {
                $("body").addClass("theme-conventional");
                $("img").each(function () {
                    if ($(this).attr("data-conventional-img")) {
                        $(this).attr("src", $(this).attr("data-conventional-img"));
                        console.log("theme conventional")
                    }
                })
            }
        },
        puzzleAnimation: function () {
            var path = $(".puzzle-path");
            path.click(function () {
                var clickedPath = $(this).attr("id");
                app._puzzleOpenAnimation(clickedPath);
                $(".puzzle-close").click(function () {
                    app._puzzleCloseAnimation(clickedPath);
                    $(".copy").unbind("click")
                })
            });
            path.on("mousemove", function () {
                var $this = $(this),
                    tooltip = $("." + $this.attr("id") + "-tooltip"),
                    offsetX = 70,
                    offsetY = 30,
                    tooltipX = event.clientX - tooltip.width() + offsetX,
                    tooltipY = event.clientY - tooltip.height() - offsetY,
                    delay = 500;
                tooltip.stop().fadeIn().css({
                    "left": tooltipX + "px",
                    "top": tooltipY + "px"
                });
                //console.log($this.offset().left);
                //console.log($this.offset().top)
            });
            path.on("mouseout", function () {
                $(".puzzle-tooltip").hide();
            });
        },
        validation: function () {
            $("input").each(function () {
                //console.log($(this).attr("data-validate"))
                var $this = $(this);
                if ($this.attr("data-validate") != undefined) {
                    $("#" + $this.attr("data-validate-trigger")).click(function (e) {
                        if ($this.val().trim() === "") {
                            $this.parents("." + $this.attr("data-parent")).addClass("error");
                            e.preventDefault();
                        } else {
                            $("." + $this.attr("data-parent")).removeClass("error");
                            app._accordionNextSlide($this);
                            app._openAnotherOverlay();
                        }
                    })
                }
                if ($this.attr("data-formating") != undefined) {
                    $this.mask($this.attr("data-formating"));
                }
            });
        },
        _puzzleOpenAnimation: function ($id) {
            $(".puzzle-main").addClass("active");
            var rightPartRight = -23;
            var rightPartWidth = 250;
            var leftPartWidth = 250;
            var rightPartBottom = 0;
            var leftPartLeft = -23;
            var leftPartTop = 0;
            var animationHeight = 739;
            //
            //width: 150px;
            //left: -3px;
            //top: 8px;
            if (app.isMobile) {
                // rightPartright =
                rightPartRight = 17;
                rightPartBottom = 17;
                leftPartLeft = -3;
                leftPartTop = 0;
                rightPartWidth = 180;
                animationHeight = 1150;
                leftPartWidth = 150;
                $(".cont-inner-left .cont-sec").slideUp();
            } else {
            }
            $(".left").stop(true, false).animate({
                width: leftPartWidth,
                left: leftPartLeft,
                top: leftPartTop
            });
            $(".puzzle-overlay,.puzzle-main ").stop(true, false).animate({
                height: animationHeight
            }, 100, function () {
                $(".puzzle-overlay").addClass("active");
                $(".right").stop(true, false).animate({
                    width: rightPartWidth,
                    right: rightPartRight,
                    bottom: rightPartBottom
                });
                $(".right").css({top: 'auto'});
                $(".puzzle-main").addClass($id);
            });
            $(".text,.puzzle-incomplete-cont").fadeOut(200);
            $(".puzzle-svg,.puzzle-shadow").hide();
            $(".puzzle-info").hide();
        },
        _puzzleCloseAnimation: function ($id) {
            var puzzleHeight = 574;
            if (app.isMobile) {
                // rightPartright =
                puzzleHeight = 174;
                $(".cont-inner-left .cont-sec").slideDown();
            }
            $(".puzzle-main").removeClass("active");
            var delay = 150;
            $(".puzzle-overlay").removeClass("active");
            $(".puzzle-main").removeClass($id);
            $(".right").delay(delay).stop(true, false).animate({
                width: "100%",
                right: 0,
                top: 0
            });
            $(".right").css({bottom: 'auto'});
            $(".left").delay(delay).stop(true, false).animate({
                width: "100%",
                left: 0,
                top: 0
            });
            $(".puzzle-main ").delay(delay).stop(true, false).animate({
                height: puzzleHeight
            }, 500, function () {
                $(".text,.puzzle-incomplete-cont").fadeIn();
                $(".puzzle-svg,.puzzle-shadow").show();
            });
            $(".puzzle-overlay").delay(delay).stop(true, false).animate({
                height: 0
            });
            $(".puzzle-info").show();
        },
        _getQueryStringParams: function (sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return decodeURI(sParameterName[1]);
                }
            }
        },
        demo: function () {
            $(" .product-category li .button").click(function () {
                var length = 0,
                    $this = $(this),
                    circle = $(".circle-bar"),
                    number = $(".progress-container2 h5"),
                    info = $(".progress-container2 #info"),
                    copy = $(".progress-container2 .copy");
                //$this.parents(".copy").find(".button").removeClass("active");
                $this.toggleClass("active");
                $(".detail-edit.incomplete-panel,#new-product-label").hide();
                $(".puzzle-main .puzzle-overlay .content .copy").each(function () {
                    if ($(this).find(".button.active").length > 0) {
                        length += 1;
                        var $id = $(this).attr("id");
                        $(".incomplete-panel." + $id).show();
                        console.log($id)
                        if ($id != "product-pay") {
                            $("#new-product-label").show();
                        }
                    }
                });
                if (length === 0) {
                    //info.text("Select 3 more to earn TouchPoints");
                } else if (length === 1) {
                    //circle.addClass("p50");
                    //info.text("Select 2 more to earn TouchPoints");
                    number.text("0");
                    //app._css3RadialProgressBar(0);
                } else if (length === 2) {
                    //app._css3RadialProgressBar(0);
                    //info.text("Select 1 more to earn TouchPoints");
                    number.text("0");
                    copy.removeClass("active");
                } else if (length === 3) {
                    //app._css3RadialProgressBar(33);
                    //info.text("");
                    number.text("2");
                    copy.addClass("active");
                    $("#progress-clr-circle").circleProgress({
                        startAngle: -Math.PI / 5.1 * 2,
                        value: 0.337,
                        animation: {
                            duration: 800
                        },
                        fill: {
                            image: 'common/images/progress/progress-circle-2.png'
                        }
                    });
                } else if (length === 4) {
                    //app._css3RadialProgressBar(67);
                    //info.text("");
                    number.text("5");
                    $("#progress-clr-circle").circleProgress({
                        startAngle: -Math.PI / 5.1 * 2,
                        animationStartValue: 0.337,
                        value: 0.68,
                        animation: {
                            duration: 800
                        },
                        fill: {
                            image: 'common/images/progress/progress-circle-3.png'
                        }
                    });
                } else if (length === 5) {
                    //app._css3RadialProgressBar(100);
                    //info.text("");
                    number.text("10");
                    $("#progress-clr-circle").circleProgress({
                        startAngle: -Math.PI / 5.1 * 2,
                        animationStartValue: 0.68,
                        animation: {
                            duration: 800
                        },
                        value: 1,
                        fill: {
                            image: 'common/images/progress/progress-circle-4.png'
                        }
                    });
                }
            });
        },
        _css3RadialProgressBar: function (value) {
            /* Vars */
            var xvaluenow = value; //Generates a random number (0-100) only for demonstration
            //console.log(xvaluenow)
            //var xvaluenow = 0; //Insert here a specific number (0-100) and remove the comment this var, and the above code
            var rotatenum = 'rotate(' + xvaluenow * 1.8 + 'deg)';
            var progress = document.getElementById('progress');
            var progress_circle = document.getElementById('progress-circle');
            var progress_style = document.getElementById('progress-style');
            /* Fix: Cover gap with shadow */
            if (xvaluenow == 0) {
                var shadowfix = "0";
            } else {
                var shadowfix = "1px";
            }
            /* Inserting values */
            //progress.innerHTML = xvaluenow + '%';
            progress_circle.setAttribute("data-valuenow", xvaluenow);
            progress_style.innerHTML = " \
.p-h:before, .p-f, .p-f:before{ \
-moz-transform: " + rotatenum + "; \
-webkit-transform: " + rotatenum + "; \
-o-transform: " + rotatenum + "; \
-ms-transform: " + rotatenum + "; \
transform: " + rotatenum + "; \
-webkit-box-shadow: 0 0 0 " + shadowfix + " #ef0208; \
  box-shadow: 0 0 0 " + shadowfix + " #ef0208;}\
\ ";
        },
        incNDecFunc: function () {
            var defSelector = $('.incNDecFunc'),
                counterPlaceholder = $('.counter-circle'),
                activeClassName = 'active',
                defaultValue = [],
                range = 10;
            for (var i = 0; i < defSelector.length; i++) {
                defaultValue[i] = 0;
            }
            defSelector.find('.inc').click(function () {
                var elemIndex = $(this).parents('.incNDecFunc').index();
                if ($(this).parent().find(counterPlaceholder).text() < range) {
                    $(this).parent().find(counterPlaceholder).addClass(activeClassName);
                    defaultValue[elemIndex] = defaultValue[elemIndex] + 1;
                    $(this).parent().find(counterPlaceholder).text(defaultValue[elemIndex]);
                }
            });
            defSelector.find('.dec').click(function () {
                var elemIndex = $(this).parents('.incNDecFunc').index();
                if ($(this).parent().find(counterPlaceholder).text() == 1) {
                    $(this).parent().find(counterPlaceholder).removeClass(activeClassName);
                }
                if ($(this).parent().find(counterPlaceholder).text() > 0) {
                    defaultValue[elemIndex] = defaultValue[elemIndex] - 1;
                    $(this).parent().find(counterPlaceholder).text(defaultValue[elemIndex]);
                }
            });
        },
        percentageCounter: function () {
            var defSelector = $('.percentage-counter'),
                counterPlaceholder = $('#loan-payment'),
                defaultValue = parseInt($('#loan-payment').val()),
                multiplicationValue = 10,
                maxRange = "80",
                minRange = 20;
            defSelector.find('.inc').click(function () {
                if (defaultValue < maxRange) {
                    var getNewValue = defaultValue + multiplicationValue;
                    $(this).parent().find(counterPlaceholder).val(getNewValue + " %");
                    defaultValue = getNewValue;
                }
            });
            defSelector.find('.dec').click(function () {
                if (defaultValue > minRange) {
                    var getNewValue = defaultValue - multiplicationValue;
                    $(this).parent().find(counterPlaceholder).val(getNewValue + " %");
                    defaultValue = getNewValue;
                }
            });
        },
        yearCounter: function () {
            var yeSelector = $('.year-counter'),
                counterPlaceholder = $('#year-payment'),
                defaultValue = 1,
                range = "20";
            yeSelector.find('.inc').click(function () {
                var currentValue = parseInt(counterPlaceholder.val(), 10);
                if (currentValue < range) {
                    defaultValue++;
                    var incValue = defaultValue * 1;
                    $(this).parent().find(counterPlaceholder).val(incValue);
                }
            });
            yeSelector.find('.dec').click(function () {
                var currentValue = parseInt(counterPlaceholder.val(), 10);
                if (currentValue > 0) {
                    defaultValue--;
                    var incValue = defaultValue * 1;
                    $(this).parent().find(counterPlaceholder).val(incValue);
                }
            });
        },
        saveOptYesNo: function () {
            //var str = $('.cont-sec h2:first').html();
            //if ($('.cont-sec h2').length >= 1) {
              //  $('.cont-sec h2:first').html(str.toLowerCase());
                //var stri = $('.container h2:first').html();
                //$('.container h2:first').html(stri.toLowerCase());
            //}
            //$(".opt,.licontent").hide();
            $(".panel-body").find("#yes").click(function () {
                $(".opt").hide();
                $("div.yes").show();
            });
            $(".panel-body").find("#no").click(function () {
                $(".opt").hide();
                $("div.no").show();
            });
            $("a.off-btn").click(function () {
                $("div.on-div").hide();
                $("div.off-div").show();
            });
            $("a.on-btn").click(function () {
                $("div.off-div").hide();
                $("div.on-div").show();
            });
            $("div.off-div .off-yes").click(function () {
                $("div.off-div .center-con .btn-no").hide();
                $("div.off-div .center-con .btn-yes").show();
            });
            $("div.off-div .off-no").click(function () {
                $("div.off-div .center-con .btn-yes").hide();
                $("div.off-div .center-con .btn-no").show();
            });
            $("div.on-div .on-yes").click(function () {
                $("div.on-div .center-con .btn-no").hide();
                $("div.on-div .center-con .btn-yes").show();
            });
            $("div.on-div .btn-yes").click(function () {
                $("div.on-div .center-con .btn-no").hide();
                $("div.on-div .center-con .btn-yes").show();
                $(".benefits .ul-no").hide();
                $(".benefits .ul-yes").show();
            });
            $("div.on-div .btn-no").click(function () {
                $("div.on-div .center-con .btn-yes").hide();
                $("div.on-div .center-con .btn-no").show();
                $(".benefits .ul-yes").hide();
                $(".benefits .ul-no").show();
            });
            $(".on-div .sb-set-1 a.btn-yes").click(function () {
                $(".benefits .ul-no").hide();
                $(".benefits .ul-yes").show();
            });
            $(".on-div .sb-set-1 a.btn-no").click(function () {
                $(".benefits .ul-yes").hide();
                $(".benefits .ul-no").show();
            });
            /*$("body").find("table").each(function (a) {
             if ($(this).find("tr:first-child th").length == 5) {
             $(this).addClass("table4");
             }
             if ($(this).find("tr:first-child th").length == 3) {
             $(this).addClass("table3");
             }
             if ($(this).find("tr:first-child th").length == 2) {
             $(this).addClass("table2");
             }
             });*/
            $("div.dynamicID > div.opt").find(".benefits").hide();
            $("div.dynamicID > div.opt").each(function () {
                $(this).find(".benefits:first").show();
            });
            $("div.benefits ul li").each(function () {
                var $this = $(this);
                var $tick = $this.find("a.tick");
                if ($this.find("a.tick").length >= 1) {
                    $this.addClass("pointer");
                }
                $this.click(function () {
                    var $content = $this.find("div.licontent");
                    $content.slideToggle("600", function () {
                        $tick.toggleClass('active', $content.is(':visible'));
                    });
                });
            });
            var $index = 1;
            $("div.dynamicID > div.opt,.cc .dynamicID > div").each(function (i) {
                $(this).addClass('opbtn-' + i);
            });
            $(".dynamic-tab > div").find(".center-con").each(function (i) {
                $(this).attr("id", "btnlog-" + i);
            });
            $("div[id^='btnlog-']").each(function (i) {
                var $this = $(this);
                $this.find("a").each(function (e) {
                    $(this).attr('data-id', 'opbtn-' + e);
                });
            });
            $(".panel-group .center-con a").click(function () {
                var $this = $(this);
                var $getID = $this.attr("data-id");
                if ($this.parents("div.panel-default").next("div.panel-default").length == 1) {
                    $(".insure-tab").find("div.dynamicID > div.opt").hide();
                    $("div.dynamicID").find("." + $getID).show();
                }
                if ($(this).hasClass('ofshorenoyes') == true) {
                    $("#headchk:visible").hide();
                }
                if ($this.parents("div.dynamicID").find("div.opt").length >= 1) {
                    if ($this.hasClass("disabled"))return;
                    var $toshow = $this.index();
                    $this.parents("div.opt").find("div.benefits").hide();
                    $this.parents("div.opt").find("div.benefits").eq($toshow).show();
                    $(this).parent().children().removeClass('active');
                    $(this).addClass('active');
                }
            });
            $('.panel-group .center-con a').click(function () {
                console.log("test505");
                switch ($(this).attr('rel')) {
                    case 'offshore':
                        $("div[id^='heading']").parent().hide();
                        $("#headingOne").parent().show();
                        $("#headingThree").parent().show().find("a").trigger("click");
                        $("#headingThree").parent().removeClass("hide");
                        $("#onshore-no").hide();
                        $("#offshore").show();
                        $(".panel-group .center-con a").removeClass("active");
                        $(".panel-group .center-con a:first-child").addClass("active");
                        $("#offshore .benefits").hide();
                        $("#offshore .benefits:first").show();
                        break;
                    case 'onshore-no':
                        $("#headingThree").parent().show().find("h4 a").trigger("click");
                        $("#headingThreeA").parent().addClass("hide");
                        $("#headingThree").parent().removeClass("hide");
                        $(".dynamicID > div.opt").hide();
                        $("#onshore-no").show();
                        break;
                }
            });
            $("div.cc div.dynamicID").each(function () {
                $(this).find(".benefits:first").show();
            });
            $(".cc > a").each(function (i) {
                var $this = $(this);
                $(this).attr('data-id', 'opbtn-' + i);
            });
            $(".cc div.dynamicID > div:first-child").show();
            $(".cc .cards").click(function () {
                var $getID = $(this).attr("data-id");
                $(".cc div.dynamicID > div").hide();
                $(".cc div.dynamicID").find("." + $getID).show();
            });
            /*table content update*/
            //$(".cont-inner .bundle-inner table").each(function (a) {
            var $table = $(".cont-inner .bundle-inner table tbody tr:first-child");
            var $heading0 = $table.children('td:eq(0)').find("p").html();
            var $heading1 = $table.children('td:eq(1)').find("p").html();
            var $heading2 = $table.children('td:eq(2)').find("p").html();
            var $heading3 = $table.children('td:eq(3)').find("p").html();
            var $heading4 = $table.children('td:eq(4)').find("p").html();
            $(".bundle-table-cont").find("div.bundle-wrap:nth-child(1) p").html($heading0);
            $(".bundle-table-cont").find("div.bundle-wrap:nth-child(2) p").html($heading1);
            $(".bundle-table-cont").find("div.bundle-wrap:nth-child(3) p").html($heading2);
            $(".bundle-table-cont").find("div.bundle-wrap:nth-child(4) p").html($heading3);
            $(".bundle-table-cont").find("div.bundle-wrap:nth-child(5) p").html($heading4);
            //});
        },
        accordionNextSlide: function () {
            $('.insure-tab .panel-heading .panel-title, .save-tab .panel-heading .panel-title').addClass('no-click');
            $('.insure-tab .panel-default, .save-tab .panel-default').eq(0).find('.panel-title').removeClass('no-click');
            $('.next-accr-open').click(function () {
                if ($(this).hasClass("disabled") == true) {
                    return false;
                }
                else if ($(this).is('[rel]') == true) {
                    $(this).parent().children().removeClass('active');
                    $(this).addClass('active');
                    return false;
                }
                else if ($(this).hasClass("skip") == true) {
                    app._accordionNextSlideSkip($(this));
                }
                else {
                    $(this).parent().children().removeClass('active');
                    $(this).addClass('active');
                    $('.credit-card-tab .cards-con').hide();
                    var index = $(this).index();
                    $(this).parents('.panel-default').next().find('.cards-con').eq(index - 1).show();
                    $(".cc div.dynamicID").find(".benefits:first-child").show();
                    app._accordionNextSlide($(this));
                }
            });
        },
        _accordionNextSlide: function (value) {
            $(value).parents('.panel-default').next().show();
            $(value).parents('.panel-default').next().removeClass("hide");
            $(value).parents('.panel-default').next().find('.panel-title').find('a').trigger("click");
            $(value).parents('.panel-default').next().find('.panel-title').removeClass('no-click');
        },
        _accordionNextSlideC: function (value) {
            $(value).parents('.panel-default').next().show();
            $(value).parents('.panel-default').next().find('.panel-title').find('a').trigger("click");
            $(value).parents('.panel-default').next().find('.panel-title').removeClass('no-click');
        },
        _accordionNextSlideSkip: function (value) {
            $(value).parents('.panel-default').next().hide();
            $(value).parents('.panel-default').next().next().find('.panel-title').find('a').trigger("click");
            $(value).parents('.panel-default').next().next().find('.panel-title').removeClass('no-click');
            $(value).parents('.panel-default').next().next().find('.dynamicID > div:first').show();
        },
        cardsSelect: function () {
            $(".credit-card-tab .cards").click(function () {
                //app._accordionNextSlide($(this));
                $(".credit-card-tab .cards").removeClass('active');
                $(this).addClass('active');
            });
            $("a.next2").click(function () {
                app._accordionNextSlide($(this));
            });
        },
        comboSelectingFilter: function () {
            $('#select-account-type li a').on('click', function () {
                $('.field-block-single-col .select-type-box').hide();
                $('.field-block-single-col .button').hide();
                var selectValue = $(this).attr('class');
                $("#" + selectValue).show();
                $("." + selectValue + "-button").show();
            });
        },
        customDropDown: function () {
            var customDropDownContainer = $(".custom-drop-down-container"),
                defaultActiveState = $(".dd-default-state"),
                customDropDownSelector = $('.custom-drop-down'),
                customDropDownListSelector = customDropDownSelector.find('li'),
                getFirstList = customDropDownSelector.find('li').eq(0),
                ContainerActiveClass = 'active',
                listHideOrActiveClass = 'list-active';
            getFirstList.addClass('hide');
            customDropDownContainer.click(function () {
                $(this).find(customDropDownSelector).toggle();
                $(this).toggleClass(ContainerActiveClass);
            });
            customDropDownListSelector.click(function () {
                var ddListTextTextSelector = $("a"),
                    activeTextArea = defaultActiveState,
                    ddListActiveTextText = $(this).find(ddListTextTextSelector).text();
                customDropDownListSelector.removeClass(listHideOrActiveClass);
                activeTextArea.text("");
                activeTextArea.append(ddListActiveTextText);
                $(this).addClass(listHideOrActiveClass);
                customDropDownListSelector.removeClass('hide');
                $(this).addClass('hide');
            });
            $("html").click(function () {
                customDropDownContainer.removeClass("active");
                customDropDownSelector.hide();
            });
            customDropDownContainer.click(function (e) {
                e.stopPropagation();
            });
        },
        sidebarScrollFocus: function () {
            var selector = $('.accordion-fill .panel-heading .accordion-toggle');
            if (selector.length >= 1) {
                var selectorOffset = selector.offset().top;
                $('.side-panel').animate({
                    //scrollTop: selectorOffset - 90
                }, 600);
            }
        },
        accordionAppend: function (targetId, hideId) {
            $(targetId).removeClass('hidden');
            $(hideId).addClass('hidden');
            console.log($(targetId));
            console.log($(hideId));
            $(targetId).find('.panel-title').find('a').trigger("click");
            $(targetId).find('.panel-title').removeClass('no-click');
        },
        randomFunction: function () {
            $('.checkout-mob .less-n-more-btn').click(function () {
                var $this = $(this);
                $this.parent().find('.toggle-box').slideToggle();
                $this.toggleClass('active');
                if ($this.hasClass('active')) {
                    $this.text('less');
                } else {
                    $this.text('more');
                }
            });
            $('#agent-code-validation').click(function () {
                var scrollValue = $(".accordion .agent").offset().top;
                $(".side-panel").animate({
                    scrollTop: scrollValue
                }, 2000);
                checkDropDownValidation()
            });
            $('.drop-down-checkout select').change(function () {
                checkDropDownValidation()
            });
            function checkDropDownValidation() {
                if ($('.drop-down-checkout select').val() == "") {
                    $('.drop-down-checkout .custom-select').addClass("error");
                } else {
                    $('.drop-down-checkout .custom-select').removeClass("error");
                }
            }

            $('.checkout').parents('body').find('.log-count').hide();
            //$('.checkout .total td:first-child, .checkout .total td:last-child,       .checkout .tfoot th:first-child, .checkout .tfoot th:last-child   ').attr('colspan', 2);
            $('header .log-count').click(function () {
                var scrollValue = $(".side-panel .accordion-fill").offset().top;
                $(".side-panel").animate({
                    scrollTop: scrollValue
                }, 600);
            });
        },
        bxSlider: function () {
            var productSlider = $('.products-slider');
            if ($(window).width() <= 540) {
                $(productSlider).bxSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    pager: false,
                    controls: false
                });
            } else {
                $(productSlider).bxSlider({
                    minSlides: 3,
                    maxSlides: 3,
                    slideWidth: 456,
                    pager: false
                });
            }
        },
        overlay: function () {
            $('.open-overlay').click(function () {
                overlayScrollClass()
            });
            function overlayScrollClass() {
                $('body').addClass('no-scroll');
                $('#submit-overlay').fadeIn();
            }

            if ($('.overlay').is(":visible")) {
                overlayScrollClass();
            }
            $('.overlay .close-btn, .overlay, .overlay .cancel-overlay').click(function () {
                $('.overlay').fadeOut();
                $('body').removeClass('no-scroll');
            });
            $('.overlay .overlay-cont').click(function (e) {
                e.stopPropagation();
            });
        },
        activeState: function () {
            $('.active-state').click(function () {
                /*
                 $(this).parents('.panel-body').find('.hide-n-show-box').removeClass('hide');*/
                $(this).parent().children().removeClass('active');
                $(this).addClass('active');
                $('.hide-n-show-box').addClass('hide');
                var index = $(this).index();
                $(this).parents('.panel-body').find('.hide-n-show-box').eq(index - 1).removeClass('hide');
            });
            $('.next-accr-open').click(function () {
                //  window.dispatchEvent(new Event('resize'));
                //    $(window).trigger("resize")
                if (app.isIe) {
                    var evt = window.document.createEvent('UIEvents');
                    evt.initUIEvent('resize', true, false, window, 0);
                    window.dispatchEvent(evt);
                }
                else {
                    window.dispatchEvent(new Event('resize'));
                }
                $(this).parents('.panel-body').find('.hide-n-show-box').addClass('hide');
            });
        },
        dropDownForCheckout: function () {
            $('#select-chanel').change(function () {
                var currentValue = $(this).val();
                $('.dd-checkout-box').hide();
                $('#' + currentValue).show();
            });
            $('#overlay-select-id').change(function () {
                var currentValue = $(this).val();
                $('.overlay-comb-hide').hide();
                $('#' + currentValue).show();
            });
        },
        monthlyIncome: function () {
            var selector = $('#monthly-income');
            $(".banner-content #next").click(function (e) {
                if (selector.val() <= 4999) {
                    $('.banner-content .alert-validation').css('visibility', 'visible');
                    selector.parents().addClass("error");
                    e.preventDefault();
                } else {
                    $('.banner-content .alert-validation').css('visibility', 'hidden');
                    selector.parents().removeClass("error");
                }
            })
        },
        cardCarouselMobile: function () {
            function cardCarouselMobile(selector) {
                if (window.innerWidth <= 767) {
                    var imagePathArray = [],
                        i = 0,
                        sliderSelector = null,
                        sliderSelector = $(selector),
                        carouselContainerCommon = $('.carousel-container-common');
                    $('.carousel-container-common').removeAttr('id');
                    $('.carousel-inner-common').show();
                    sliderSelector.parent().addClass('has-card-carousel-mobile-wrapper');
                    sliderSelector.find('li[data-thumbnail-path]').each(function () {
                        var imagePath = $(this).data("thumbnail-path");
                        imagePathArray[i++] = imagePath;
                    });
                    sliderSelector.find('ul').remove();
                    carouselContainerCommon.remove();
                    for (var n = 0; n < imagePathArray.length; n++) {
                        sliderSelector.append('<li><img src="' + imagePathArray[n] + '"  />');
                    }
                    sliderSelector.bxSlider({
                        minSlides: 1,
                        maxSlides: 1,
                        pager: false,
                        onSlideNext: function ($slideElement) {
                            sliderSelector.parents('.bx-wrapper').next('.hide-n-show-info-wrapper').find('.benefits').hide();
                            $slideElement.parents('.bx-wrapper').next('.hide-n-show-info-wrapper').find('.benefits').eq(sliderSelector.getCurrentSlide()).show();
                        },
                        onSlidePrev: function ($slideElement) {
                            sliderSelector.parents('.bx-wrapper').next('.hide-n-show-info-wrapper').find('.benefits').hide();
                            $slideElement.parents('.bx-wrapper').next('.hide-n-show-info-wrapper').find('.benefits').eq(sliderSelector.getCurrentSlide()).show();
                        }
                    });
                }
            }

            $('.next-accr-open').click(function () {
                var index = $(this).index();
                switch (index) {
                    case 0:
                        console.log(" 0 Mobile Carousel");
                        cardCarouselMobile('#cc-touch-points-carousel');
                        cardCarouselMobile('#cc-islamic-touchpoint-carousel');
                        cardCarouselMobile('#etihad-carousel');
                        break;
                    case 1:
                        console.log(" 1 Mobile Carousel");
                        cardCarouselMobile('#cc-traveller-carousel');
                        break;
                    case 2:
                        console.log(" 2 Mobile Carousel");
                        cardCarouselMobile('#cc-etihad-carousel');
                        break;
                    case 3:
                        console.log(" 3 Mobile Carousel");
                        cardCarouselMobile('#cc-lulu-carousel');
                        break;
                }
            });
            /*cardCarouselMobile('#touch-points-carousel');
             cardCarouselMobile('#etihad-carousel');
             cardCarouselMobile('#lulu-carousel');
             cardCarouselMobile('#traveller-carousel');
             */
            cardCarouselMobile('#etihad-guest-carousel');
            cardCarouselMobile('#ca-etihad-and-touchpoint-carousel');
        },
        demoOverlay: function () {
            $("#confirmation-overlay, #sure-overlay").hide();
            $("body").removeClass("no-scroll");
            $(".overlay-open a").click(function (e) {
                e.stopPropagation();
                $("#confirmation-overlay, #sure-overlay").show();
            });
        },
        _openAnotherOverlay: function () {
            $('#submit-overlay').fadeOut();
            setTimeout(function () {
                $('#thank-you-overlay').fadeIn();
            }, 50);
        },
        init: function () {
            app.theme();
            app.saveOptYesNo();
            app.detectDevice();
            app.resizeListener();
            app.msIeVersion();
            app.eventListeners();
            app.validation();
            app.puzzleAnimation();
            app.demo();
            app.incNDecFunc();
            app.accordionNextSlide();
            app.percentageCounter();
            app.cardsSelect();
            app.yearCounter();
            app.comboSelectingFilter();
            app.customDropDown();
            app.randomFunction();
            app.bxSlider();
            app.overlay();
            app.activeState();
            app.dropDownForCheckout();
            app.monthlyIncome();
            app.cardCarouselMobile();
            app.demoOverlay();
        }
    };
    window.app = app;
})(window, document, jQuery);
$(document).ready(function () {
    app.init();
});
FWDRLS3DUtils.onReady(function () {
    function initSlider(id, visibleSlideNumbers, mainVariableParam, slideVariableParam) {
        if ($("#" + id + "-carousel-container").length >= 1) {
            var variableValue = mainVariableParam,
                slideVariableValue = slideVariableParam,
                slideNumberDivided = null,
                variableValue = null,
                slideVariableValue = "#" + id + "-carousel-container" + " > div > div:nth-child(3) > div:nth-child(1) > div > div";
            if (visibleSlideNumbers == 20) {
                slideNumberDivided = 9;
            } else if (visibleSlideNumbers == 19) {
                slideNumberDivided = 9;
            } else if (visibleSlideNumbers == 18) {
                slideNumberDivided = 8;
            } else if (visibleSlideNumbers == 17) {
                slideNumberDivided = 8;
            } else if (visibleSlideNumbers == 16) {
                slideNumberDivided = 7;
            } else if (visibleSlideNumbers == 15) {
                slideNumberDivided = 7;
            } else if (visibleSlideNumbers == 14) {
                slideNumberDivided = 6;
            } else if (visibleSlideNumbers == 13) {
                slideNumberDivided = 6;
            } else if (visibleSlideNumbers == 12) {
                slideNumberDivided = 5;
            } else if (visibleSlideNumbers == 11) {
                slideNumberDivided = 5;
            } else if (visibleSlideNumbers == 10) {
                slideNumberDivided = 4;
            } else if (visibleSlideNumbers == 9) {
                slideNumberDivided = 4;
            } else if (visibleSlideNumbers == 8) {
                slideNumberDivided = 3;
            } else if (visibleSlideNumbers == 7) {
                slideNumberDivided = 3;
            } else if (visibleSlideNumbers == 6) {
                slideNumberDivided = 2;
            } else if (visibleSlideNumbers == 5) {
                slideNumberDivided = 2;
            } else if (visibleSlideNumbers == 4) {
                slideNumberDivided = 1;
            } else if (visibleSlideNumbers == 3) {
                slideNumberDivided = 1;
            } else if (visibleSlideNumbers == 2) {
                slideNumberDivided = 0;
            } else if (visibleSlideNumbers == 1) {
                slideNumberDivided = 0;
            } else {
                slideNumberDivided = 0;
            }
            $("#" + id + "-carousel-container").parent().find('.hide-n-show-info-wrapper').addClass("carousel-slides-" + visibleSlideNumbers);
            $("#" + id + "-carousel-container").siblings('.hide-n-show-info-wrapper').attr('id', id + "-info-div");
            if (window.innerWidth >= 768) {
                variableValue = new FWDSimple3DCoverflow({
                    //required settings
                    coverflowHolderDivId: id + "-carousel-container",
                    coverflowDataListDivId: id + "-carousel",
                    displayType: "responsive",
                    autoScale: "yes",
                    coverflowWidth: 890,
                    coverflowHeight: 200,
                    mainFolderPath: "../Common/carousel-plugin",
                    skinPath: "skin_white",
                    //main settings
                    showDisplay2DAlways: "no",
                    coverflowStartPosition: "center",
                    coverflowTopology: "dualsided",
                    coverflowXRotation: 0,
                    coverflowYRotation: 0,
                    numberOfThumbnailsToDisplayLeftAndRight: slideNumberDivided,
                    infiniteLoop: "yes",
                    useDragAndSwipe: "no",
                    fluidWidthZIndex: 1000,
                    //thumbnail settings
                    thumbnailWidth: 400,
                    thumbnailHeight: 200,
                    thumbnailXOffset3D: 100,
                    thumbnailXSpace3D: 100,
                    thumbnailZOffset3D: 80,
                    thumbnailZSpace3D: 93,
                    thumbnailYAngle3D: 2,
                    thumbnailXOffset2D: 20,
                    thumbnailXSpace2D: 30,
                    thumbnailBorderSize: 0,
                    transparentImages: "yes",
                    thumbnailsAlignment: "center",
                    maxNumberOfThumbnailsOnMobile: 13,
                    showThumbnailsGradient: "no",
                    showText: "no",
                    textOffset: 10,
                    showThumbnailBoxShadow: "no",
                    thumbnailBoxShadowCss: "0px 0 220px 60px #fff",
                    showTooltip: "no",
                    dynamicTooltip: "no",
                    showReflection: "yes",
                    reflectionHeight: 50,
                    reflectionDistance: 0,
                    reflectionOpacity: .1,
                    //controls settings
                    slideshowDelay: 5000,
                    autoplay: "no",
                    disableNextAndPrevButtonsOnMobile: "no",
                    controlsMaxWidth: 650,
                    controlsPosition: "bottom",
                    controlsOffset: 10,
                    showLargeNextAndPrevCoverflowButtons: "yes",
                    largeNextAndPrevButtonsOffest: 20,
                    showNextAndPrevCoverflowButtons: "no",
                    showSlideshowButton: "no",
                    showScrollbar: "no",
                    showBulletsNavigation: "no",
                    bulletsNormalColor: "#999999",
                    bulletsSelectedColor: "#000000",
                    bulletsNormalRadius: 5,
                    bulletsSelectedRadius: 8,
                    spaceBetweenBullets: 10,
                    bulletsOffset: 18,
                    disableScrollbarOnMobile: "yes",
                    enableMouseWheelScroll: "no",
                    scrollbarHandlerWidth: 200,
                    scrollbarTextColorNormal: "#000000",
                    scrollbarTextColorSelected: "#FFFFFF",
                    addKeyboardSupport: "yes",
                    showNextAndPrevButtonsOnMobile: "yes"
                });
                variableValue.addListener(FWDSimple3DCoverflow.THUMB_CHANGE, onThumbChange);
                variableValue.addListener(FWDSimple3DCoverflow.IS_API_READY, function () {
                        $(slideVariableValue).eq(slideNumberDivided).addClass("active-slide");
                    }
                );
                function onThumbChange(ev) {
                    $(slideVariableValue).removeClass("active-slide");
                    $(slideVariableValue).eq(ev.id).addClass("active-slide");
                    $("#" + id + "-info-div .benefits").hide();
                    $("#" + id + "-info-div .benefits").eq(ev.id).show();
                    console.log(variableValue.getCurrentThumbId() + 1);
                }
            }
        }
    }

    // initSlider( id, visibleSlideNumbers, mainVariableParam, slideVariableParam );
    initSlider("cc-touch-points", 8, "creditCard", "creditCardSlide");
    initSlider("cc-islamic-touchpoint", 4, "creditCardIslamic", "creditCardIslamicSlide");
    initSlider("cc-etihad", 4, "creditCard2", "creditCardSlide2");
    initSlider("cc-lulu", 3, "creditCard3", "creditCardSlide3");
    initSlider("cc-traveller", 1, "creditCard4", "creditCardSlide4");
    initSlider("etihad-guest", 2, "currentAccount", "currentAccountSlide");
    initSlider("ca-touchpoint", 3, "currentAccount", "currentAccountSlide");
    initSlider("ca-etihad-guest", 3, "currentAccount2", "currentAccountSlide2");
    initSlider("ca-etihad-and-touchpoint", 6, "currentAccount3", "currentAccountSlide3");
    initSlider("test", 1, "test2", "testSlide2");
		
	
});